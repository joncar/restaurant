<div class="row">
    <?php if($pedidos->facturado==0): ?>
    <div class="col-xs-12 col-sm-6" id="productosTable">
        <div class="panel panel-default">
            <div class="panel-heading"><h1 class="panel-title">Productos</h1></div>        
            <div class="panel-body" style="padding:0px;">
                <?= $productos->output ?>
            </div>
        </div>
    </div>
    <?php endif ?>
    <div class="col-xs-12 col-sm-<?= $pedidos->facturado==0?'6':'12' ?>" id="pedidoTable">
        <?php if($pedidos->facturado==0): ?>
        <p>
            <input type="checkbox" id="todos"> <label for="todos">Marcar todos los productos</label>            
        </p>
        <?php endif ?>
        <div class="panel panel-default">
            <div class="panel-heading"><h1 class="panel-title">Pedido</h1></div>       
            <div class="panel-body" style="padding:0px;">                
                <?= $output ?>
            </div>       
        </div>
    </div>
</div>
<script>
    $('#productosTable .flexigrid').on('success',function(){          
        $("#pedidoTable .ajax_refresh_and_loading").trigger('click');
    });
    $(document).on('keydown','.cantidadProducto',function(e){              
        if(e.which===13){
            e.preventDefault();
            $(this).parents('tr').find(".refreshlist").trigger('click');
        }
    });
    $(document).on('click',".refreshlist",function(){
        var x = $(this).parents('tr').find('.cantidadProducto');
        if(x.val()===''){
            x.val(1);
        }
        $("#productosTable .filtering_form").submit();
    });
</script>