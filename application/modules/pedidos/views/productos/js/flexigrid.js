$(function(){        
    $('.ajax_refresh_and_loading_producto').click(function(){

            $(this).parents('.filtering_form').trigger('submit');
    });
});

var onsendits = false;
function filterSearchClickProducto(datos){
    if(!onsendits){
        onsendits = true;        
        var contentForm = $(datos);
        var flexigrid = $(datos).find('.flexigrid');
        var formdatos = datos;
        $(".ajax_refresh_and_loading_producto").addClass('loading');
        var ajax_list_info_url = $(datos).attr('data-ajax-list-info-url');
        var ajax_list = $(datos).attr('action');
        d = new FormData(formdatos);
        d.append('page',crud_pagin);
        $.ajax({
            url:ajax_list_info_url,
            data:d,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success:function(data){
                data = JSON.parse(data);
                flexigrid.find('.total_items').html( data.total_results);                    
                total_results = data.total_results;         
                displaying_and_pages(flexigrid);
                d = new FormData(formdatos);
                d.append('page',crud_pagin);
                $.ajax({
                    url:ajax_list,
                    data:d,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success:function(data){
                        flexigrid.find('.ajax_list').html(data);
                        $(".ajax_refresh_and_loading_producto").removeClass('loading');
                        $("#pedidoTable .ajax_refresh_and_loading_producto").trigger('click');                        
                        call_fancybox();
                        add_edit_button_listener();
                        flexigrid.trigger('success');  
                        onsendits = false;
                    },
                    error:function(data){                    
                        error_message('Ha ocurrido un error consultando los datos, ERROR: '+data.status);
                        $(".ajax_refresh_and_loading_producto").removeClass('loading');
                        onsendits = false;
                    }
                });
            },
            error:function(data){                    
                error_message('Ha ocurrido un error consultando la información de los datos, ERROR: '+data.status);
                $(".ajax_refresh_and_loading_producto").removeClass('loading');
                onsendits = false;
            }
        });
    }
    return false;
}

$(document).on('ready',function(){
    $('.ajax_refresh_and_loading_producto').trigger('click');
});