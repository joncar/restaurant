<div class="widget-box transparent ui-sortable-handle" data-id="4">
   
            <div class="widget-header">
                <h5 class="widget-title"><i class="ace-icon fa fa-star orange"></i> Accesos directos</h5>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <!--<a data-toggle="dropdown" data-action="settings" href="#">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
                            <li>
                                <a href="#dropdown1" data-toggle="tab"><b>Año Lectivo</b></a>
                            </li>                            
                        </ul>-->
                    </div>

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="widget-main" style="text-align: center;">
                        <a href="<?= base_url('movimientos/ventas/ventas') ?>" class="btn btn-success btn-md btn-app no-radius">
                            <i class="ace-icon fa fa-shopping-cart"></i>
                            Ventas
                            <!--<span class="badge badge-pink">+3</span>-->
                          </a>

                          <a href="<?= base_url('movimientos/compras/compras') ?>" class="btn btn-success btn-md btn-app no-radius">
                            <i class="ace-icon fa fa-id-card"></i>
                            Compras
                            <!--<span class="badge badge-pink">+3</span>-->
                          </a>
                          <a href="<?= base_url('movimientos/productos/Transferencias') ?>" class="btn btn-success btn-md btn-app no-radius">
                            <i class="ace-icon fa fa-retweet"></i>
                            Transf.
                            <!--<span class="badge badge-pink">+3</span>-->
                          </a>
                          <a href="<?= base_url('movimientos/productos/productos') ?>" class="btn btn-purple btn-md btn-app no-radius">
                            <i class="ace-icon fa fa-cubes"></i>
                            Productos
                            <!--<span class="badge badge-pink">+3</span>-->
                          </a>
                          <a href="<?= base_url('movimientos/productos/inventario') ?>" class="btn btn-purple btn-md btn-app no-radius">
                            <i class="ace-icon fa fa-database"></i>
                            Inventario
                            <!--<span class="badge badge-pink">+3</span>-->
                          </a>
                          <a href="<?= base_url('cajas/admin/gastos') ?>" class="btn btn-danger btn-md btn-app no-radius">
                            <i class="ace-icon fa fa-money"></i>
                            Gastos
                            <!--<span class="badge badge-pink">+3</span>-->
                          </a>
                          <a href="<?= base_url('movimientos/creditos/creditos') ?>" class="btn btn-danger btn-md btn-app no-radius">
                            <i class="ace-icon fa fa-percent"></i>
                            Créditos
                            <!--<span class="badge badge-pink">+3</span>-->
                          </a>
                          <a href="<?= base_url('movimientos/creditos/creditos') ?>" class="btn btn-primary btn-md btn-app no-radius">
                            <i class="ace-icon fa fa-retweet"></i>
                            Sucursal
                            <!--<span class="badge badge-pink">+3</span>-->
                          </a>
                          <a href="<?= base_url('panel/selcaja') ?>" class="btn btn-primary btn-md btn-app no-radius">
                            <i class="ace-icon fa fa-retweet"></i>
                            Caja
                            <!--<span class="badge badge-pink">+3</span>-->
                          </a>
                          <a href="<?= base_url('panel/selcajadiaria') ?>" class="btn btn-primary btn-md btn-app no-radius">
                            <i class="ace-icon fa fa-retweet"></i>
                            C.Diaria
                            <!--<span class="badge badge-pink">+3</span>-->
                          </a>
                          <a href="<?= base_url('seguridad/perfil/edit/'.$this->user->id) ?>" class="btn btn-warning btn-md btn-app no-radius">
                            <i class="ace-icon fa fa-user"></i>
                            Perfil
                            <!--<span class="badge badge-pink">+3</span>-->
                          </a>
                    </div>
                </div>
            </div>
</div>
<script>

</script>
