<div class="widget-color-dark widget-box ui-sortable-handle" data-id="4">
   
            <div class="widget-header">
                <h5 class="widget-title"><i class="ace-icon fa fa-clone"></i> Resumen de caja diaria</h5>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <!--<a data-toggle="dropdown" data-action="settings" href="#">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
                            <li>
                                <a href="#dropdown1" data-toggle="tab"><b>Año Lectivo</b></a>
                            </li>                            
                        </ul>-->
                    </div>

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="widget-main">
                        <div class="row" style="margin-left: 0; margin-right: 0">
                            <div class="col-sm-6 col-md-3" style="text-align: center">
    						  	<a href="#">
    							    <div class="thumbnail">
    							      <span>Monto inicial</span>
    							      <div class="caption">
    							        <h4><?php 
                                                echo $this->db->query("SELECT 
                                                    format(cajadiaria.monto_inicial,0, 'de_DE') as 'total'
                                                    FROM cajadiaria 
                                                    WHERE cajadiaria.id=".$this->user->cajadiaria)->row()->total;
                                            ?></h4>               
    							      </div>
    							    </div>
    							</a>
    						</div>
    						<div class="col-sm-6 col-md-offset-2 col-md-2" style="text-align: center">
    						  	<a href="#">
    							    <div class="thumbnail">
    							      <span>Total contado</span>
    							      <div class="caption">
    							        <h4>

                                            <?php 
                                                $total = $this->db->query("SELECT 
                                                format(round(sum(ventadetalle.totalcondesc)),0, 'de_DE') as total 
                                                FROM ventas 
                                                INNER JOIN ventadetalle on ventas.id = ventadetalle.venta 
                                                WHERE ventas.status = 0 and ventas.transaccion = 1  AND ventas.cajadiaria=".$this->user->cajadiaria."
                                                GROUP BY ventas.cajadiaria");
                                                echo $total->num_rows()>0?$total->row()->total:0;
                                            ?>

                                        </h4>               
    							      </div>
    							    </div>
    							</a>
    						</div>
    						<div class="col-sm-6 col-md-2" style="text-align: center">
    						  	<a href="#">
    							    <div class="thumbnail">
    							      <span>Total crédito</span>
    							      <div class="caption">
    							        <h4>

                                            <?php 
                                                $qr = $this->db->query("SELECT 
                                                format(round(sum(ventadetalle.totalcondesc)),0, 'de_DE') as total 
                                                FROM ventas 
                                                INNER JOIN ventadetalle on ventas.id = ventadetalle.venta 
                                                WHERE ventas.status = 0 and ventas.transaccion = 2 AND ventas.cajadiaria=".$this->user->cajadiaria."
                                                GROUP BY ventas.cajadiaria");
                                                echo $qr->num_rows()>0?$qr->row()->total:0;
                                            ?>

                                        </h4>               
    							      </div>
    							    </div>
    							</a>
    						</div>
    						<div class="col-sm-6 col-md-2" style="text-align: center">
    						  	<a href="#">
    							    <div class="thumbnail">
    							      <span>Total nota crédito</span>
    							      <div class="caption">
    							        <h4>
                                            <?php 
                                                $qr = $this->db->query("SELECT 
                                                format(round(SUM(notas_credito_cliente.total_monto)),0,'de_DE') as total
                                                FROM notas_credito_cliente 
                                                where notas_credito_cliente.anulado = 0 AND notas_credito_cliente.cajadiaria= ".$this->user->cajadiaria."
                                                GROUP BY notas_credito_cliente.cajadiaria");
                                                echo $qr->num_rows()>0?$qr->row()->total:0;
                                            ?>
                                        </h4>               
    							      </div>
    							    </div>
    							</a>
    						</div>
                        </div>
                        <div class="row" style="margin-left: 0; margin-right: 0">
                            <div class="col-sm-6 col-md-3" style="text-align: center">
    						  	<a href="#">
    							    <div class="thumbnail">
    							      <span>Efectivo a rendir</span>
    							      <div class="caption">
    							        <h4>
                                            <?php $qry = $this->db->query("
                                                SELECT 
                                                format((sum(efectivo.Ingreso)+sum(efectivo.Caja_inicial))-sum(efectivo.Egreso),0,'de_DE') as total 
                                                FROM(
                                                SELECT
                                                if(consulta.Tipo = 'Caja_inicial',consulta.total,0) as Caja_inicial, 
                                                if(consulta.Tipo = 'Ingreso',consulta.total,0) as Ingreso, 
                                                if(consulta.Tipo = 'Egreso',consulta.total,0) as Egreso
                                                FROM( 
                                                SELECT 
                                                'Caja_inicial' as Tipo, 
                                                'Monto_inicial' as Movimiento, 
                                                cajadiaria.monto_inicial as total 
                                                FROM 
                                                cajadiaria WHERE cajadiaria.id = ".$this->user->cajadiaria."
                                                UNION ALL 
                                                SELECT
                                                'Ingreso' as Tipo,
                                                'Venta_contado' as Movimiento,
                                                ifnull(sum(ventadetalle.totalcondesc),0) as total
                                                FROM ventas
                                                INNER JOIN ventadetalle on ventadetalle.venta = ventas.id
                                                INNER JOIN productos on productos.codigo = ventadetalle.producto
                                                WHERE ventas.transaccion = 1 and ventas.status = 0 and ventas.cajadiaria = ".$this->user->cajadiaria."
                                                UNION ALL 
                                                SELECT 
                                                'Ingreso' as Tipo, 
                                                'Entrega_credito' as Movimiento, 
                                                ifnull(sum(cr.entrega_inicial),0) as total 
                                                FROM creditos cr 
                                                INNER JOIN ventas on ventas.id = cr.ventas_id WHERE cr.anulado = 0 or cr.anulado is null and ventas.status != -1 and ventas.cajadiaria = 3
                                                UNION ALL 
                                                SELECT
                                                'Ingreso' as Tipo,
                                                'Pago_credito' as Movimiento, 
                                                ifnull(sum(total_pagado),0) as total 
                                                FROM pagocliente WHERE (anulado = 0 or anulado is null) and cajadiaria = ".$this->user->cajadiaria."
                                                UNION ALL 
                                                SELECT 
                                                'Egreso' as Tipo, 
                                                'Gastos_varios' as Movimiento, 
                                                IFNULL(sum(gastos.monto),0) as total
                                                FROM gastos WHERE 1 and gastos.cajadiaria = ".$this->user->cajadiaria.") AS consulta) AS efectivo");
                                                echo $qry->num_rows()>0?$qry->row()->total:0;
                                            ?>
                                        </h4>               
    							      </div>
    							    </div>
    							</a>
    						</div>
    						<div class="col-sm-6 col-md-offset-2 col-md-2" style="text-align: center">
    						  	<a href="#">
    							    <div class="thumbnail">
    							      <span>Total cobros</span>
    							      <div class="caption">
    							        <h4>
                                            <?php 
                                                $total = $this->db->query("SELECT 
                                                format(SUM(pagocliente.total_pagado) ,0, 'de_DE') AS total
                                                FROM pagocliente 
                                                INNER JOIN clientes on clientes.id=pagocliente.clientes_id
                                                WHERE pagocliente.cajadiaria=".$this->user->cajadiaria." and pagocliente.anulado=0
                                                GROUP BY pagocliente.cajadiaria");
                                                echo $total->num_rows()>0?$total->row()->total:0;
                                            ?>
                                        </h4>               
    							      </div>
    							    </div>
    							</a>
    						</div>
    						<div class="col-sm-6 col-md-2" style="text-align: center">
    						  	<a href="#">
    							    <div class="thumbnail">
    							      <span>Total ventas</span>
    							      <div class="caption">
    							        <h4>
                                            <?php 
                                                $qry = $this->db->query("SELECT 
                                                        format(SUM(ventadetalle.totalcondesc),0, 'de_DE') total
                                                        FROM ventas 
                                                        INNER JOIN ventadetalle on ventas.id = ventadetalle.venta 
                                                        WHERE ventas.status = 0 and ventas.cajadiaria=".$this->user->cajadiaria." 
                                                        group by ventas.cajadiaria");
                                                echo $qry->num_rows()>0?$qry->row()->total:0;
                                            ?>                     
                                        </h4>               
    							      </div>
    							    </div>
    							</a>
    						</div>
    						<div class="col-sm-6 col-md-2" style="text-align: center">
    						  	<a href="#">
    							    <div class="thumbnail">
    							      <span>Total gastos</span>
    							      <div class="caption">
    							        <h4>
                                            <?php 
                                                echo $this->db->query("SELECT COALESCE(sum(gastos.monto),0) as total FROM gastos WHERE gastos.cajadiaria=".$this->user->cajadiaria)->row()->total;
                                            ?>
                                        </h4>               
    							      </div>
    							    </div>
    							</a>
    						</div>
                        </div>

                    </div>
                </div>
            </div>
</div>
<script>

</script>
