<div class="row">
  <div class="col-xs-12">
      <?= $this->load->view('components/accesos_directos',array(),TRUE,'dashboards'); ?>
    </div>
</div>
<div class="row">
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#mesas" aria-expanded="true" aria-controls="mesas">
              <i class="fa fa-cutlery"></i> MESAS
          </a>
        </h4>
      </div>
      <div id="mesas" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12 col-xs-12" align="right">
                            <label class="label label-primary">Ocupadas</label>
                            <label class="label label-default">Disponibles</label>
                        </div>

                         <?php foreach ($this->db->get_where("mesas")->result() as $m): ?>
                                      <div class="col-lg-2 col-md-6">
                                          <a href="<?= $m->estado ? base_url("pedidos/admin/pedidos_detalles/" . $m->pedidos_id) : "javascript:reservarMesa(" . $m->id . ",'mesa')" ?>">
                                              <div class="panel panel-<?= $m->estado ? 'primary' : 'default' ?>">
                                                  <div class="panel-heading">
                                                      <div class="row">
                                                          <div class="col-xs-12 col-sm-3">
                                                              <i class="fa fa-<?= $m->estado ? 'users' : 'cutlery' ?> fa-3x"></i>
                                                          </div>
                                                          <div class="col-xs-12  col-sm-9 text-right">
                                                              <?php 
                                                                  $monto = 0;
                                                                  if($m->estado && !empty($m->pedidos_id)){
                                                                      $monto = $this->db->query("SELECT SUM(pedidos_detalles.total) as total from pedidos_detalles where pedidos_id = ".$m->pedidos_id)->row()->total;
                                                                  }
                                                              ?>
                                                              <h2 class="huge" id="espera"><?= $m->mesa_nombre ?></h2>
                                                              <div id="clientes"><?= number_format($monto,0,',','.') ?> Gs</div>                                        
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="panel-footer">
                                                      <span class="pull-left"><?= $m->estado ? 'Ver Detalles' : 'Reservar' ?></span>
                                                      <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                      <div class="clearfix"></div>
                                                  </div>                        
                                              </div>
                                          </a>
                                      </div>
                      <?php endforeach ?>


                    </div>
                </div>
      </div>
    </div>
      
      
      
      
      <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#barras" aria-expanded="true" aria-controls="barras">
              <i class="fa fa-motorcycle"></i> Deliverys
          </a>
        </h4>
      </div>
      <div id="barras" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">                
              <div class="panel-body">
                  <div class="row">
                      <div class="col-lg-12 col-xs-12" align="right">
                          <label class="label label-primary">Ocupadas</label>
                          <label class="label label-default">Disponibles</label>
                      </div>

  <?php foreach ($this->db->get_where("deliverys")->result() as
          $m): ?>
                          <div class="col-lg-3 col-md-6">
                              <a href="<?= $m->estado ? base_url("pedidos/admin/pedidos_detalles/" . $m->pedidos_id) : "javascript:reservarMesa(" . $m->id . ",'delivery')" ?>">
                                  <div class="panel panel-<?= $m->estado ? 'primary' : 'default' ?>">
                                      <div class="panel-heading">
                                          <div class="row">                                            
                                              <div class="col-xs-12 text-right">
                                                  <?php 
                                                      $monto = 0;
                                                      $cliente = '';
                                                      if($m->estado && !empty($m->pedidos_id)){
                                                          $monto = $this->db->query("SELECT SUM(pedidos_detalles.total) as total from pedidos_detalles where pedidos_id = ".$m->pedidos_id)->row()->total;
                                                          $this->db->select('clientes.*, pedidos.cliente_nombre');
                                                          $this->db->join('clientes','clientes.id = pedidos.clientes_id');
                                                          $cliente = @$this->db->get_where('pedidos',array('pedidos.id'=>$m->pedidos_id))->row();
                                                      }
                                                  ?>
                                                  <h2 class="huge">
                                                    <?= $m->nombre_delivery ?>
                                                  </h2>
                                                  <div id="clientes">
                                                          <?= number_format($monto,0,',','.') ?> Gs <br/> 
                                                          <?= @$cliente->id==1?@$cliente->cliente_nombre:@$cliente->nombres.' '.@$cliente->apellidos ?>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="panel-footer">
                                          <span class="pull-left"><?= $m->estado ? 'Ver Detalles' : 'Reservar' ?></span>
                                          <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                          <div class="clearfix"></div>
                                      </div>                        
                                  </div>
                              </a>
                          </div>
  <?php endforeach ?>


                  </div>
              </div>
      </div>
    </div>
      
      
      <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#deliverys" aria-expanded="true" aria-controls="deliverys">
              <i class="fa fa-beer"></i> Barras
          </a>
        </h4>
      </div>
      <div id="deliverys" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                   <div class="panel panel-default">
              <div class="panel-heading">
                  <h1 class="panel-title"><i class="fa fa-beer"></i> Administración de barras</h1>
              </div>
              <div class="panel-body">
                  <div class="row">
                      <div class="col-lg-12 col-xs-12" align="right">
                          <label class="label label-primary">Ocupadas</label>
                          <label class="label label-default">Disponibles</label>
                      </div>

  <?php foreach ($this->db->get_where("barras")->result() as
          $m): ?>
                          <div class="col-lg-3 col-md-6">
                              <a href="<?= $m->estado ? base_url("pedidos/admin/pedidos_detalles/" . $m->pedidos_id) : "javascript:reservarMesa(" . $m->id . ",'barra')" ?>">
                                  <div class="panel panel-<?= $m->estado ? 'primary' : 'default' ?>">
                                      <div class="panel-heading">
                                          <div class="row">                                            
                                              <div class="col-xs-12 text-right">
                                                  <?php 
                                                      $monto = 0;
                                                      if($m->estado && !empty($m->pedidos_id)){
                                                          $monto = $this->db->query("SELECT SUM(pedidos_detalles.total) as total from pedidos_detalles where pedidos_id = ".$m->pedidos_id)->row()->total;
                                                      }
                                                  ?>
                                                  <h2 class="huge"><?= $m->nombre_barra ?></h2>
                                                  <div id="clientes"><?= number_format($monto,0,',','.') ?> Gs</div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="panel-footer">
                                          <span class="pull-left"><?= $m->estado ? 'Ver Detalles' : 'Reservar' ?></span>
                                          <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                          <div class="clearfix"></div>
                                      </div>                        
                                  </div>
                              </a>
                          </div>
  <?php endforeach ?>


                  </div>
              </div>
          </div> 
      </div>
    </div>
    
  </div>
</div>
<?php $this->load->view('components/dashMetricas'); ?>
<script>
    function reservarMesa(id, tipo) {
        var data = {
            sucursales_id:<?= $this->user->sucursal ?>,
            facturado: 0,
            fecha_pedido: '<?= date("Y-m-d H:i:s") ?>',
            mozos_id: '<?= !empty($this->user->mozo) ? $this->user->mozo : 0 ?>',
            user_id:<?= $this->user->id ?>
        };

        switch (tipo) {
            case 'mesa':
                data.mesas_id = id;
                data.tipo_pedidos_id = 1;
                break;
            case 'delivery':
                data.deliverys_id = id;
                data.tipo_pedidos_id = 2;
                break;
            case 'barra':
                data.barras_id = id;
                data.tipo_pedidos_id = 3;
                break;
        }
        data.cajadiaria = '<?= $_SESSION['cajadiaria'] ?>';
        $.post("<?= base_url("pedidos/admin/pedidos/insert") ?>", data, function (data) {
            data = data.replace('<textarea>', '', data);
            data = data.replace('</textarea>', '', data);
            data = JSON.parse(data);
            if (data.success) {
                document.location.href = "<?= base_url('pedidos/admin/pedidos_detalles') ?>/" + data.insert_primary_key + '/';
            } else {
                alert("Ha ocurrido un error al reservar la mesa");
                document.location.reload();
            }
        });
    }
</script>