<?php

require_once APPPATH.'/controllers/Panel.php';    

class Compras extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }                
    }
    
    public function compras($x = '',$y = '',$z = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $crud->field_type('transaccion', 'dropdown', array('0' => 'ASD'));
        $crud->callback_column('status', function($val, $row) {
            switch ($val) {
                case '1':return 'Pagada <a title="anular" href="javascript:anular(' . $row->id . ')"><i class="glyphicon glyphicon-remove"></i></a>';
                    break;
                case '0':return 'Activa <a title="anular" href="javascript:anular(' . $row->id . ')"><i class="glyphicon glyphicon-remove"></i></a>';
                    break;
                case '-1':return 'Anulada';
                    break;
                case '-2': return 'Anulada y procesada';
                    break;
            }
        });
        $crud->callback_column('nro_factura', function($val, $row) {
            return '<a href="javascript:showDetail('.$row->id.')">'.$val.'</a>';
        });

        $crud->set_rules('total_compra','Total de compra','required|numeric|greater_than[0]');        
        $crud->set_rules('productos','Productos','required|callback_unProducto');                
        if($crud->getParameters()=='add'){
            $crud->set_rules('nro_factura','Numero de factura','required|is_unique[compras.nro_factura]');
        }else{
            $crud->set_rules('nro_factura','Numero de factura','required');
        }        
        $crud->callback_after_insert(array($this,'addBody'));

        $crud->set_relation('sucursal', 'sucursales', 'denominacion');
        $crud->order_by('compras.id','DESC');
        $crud->columns('nro_factura','sucursal','fecha','tipo_facturacion_id','proveedor','total_compra','status');
        $output = $crud->render();
        $output->crud = 'compras';
        if ($crud->getParameters()!='list'){
            $edit = '';
            if($crud->getParameters()=='edit'){
                $edit = $y;                
            }
            $output->output = $this->load->view('compras_bew',array('output'=>$output->output,'edit'=>$edit),TRUE);            
        }else{
            $output->output = $this->load->view('compras_list',array('output'=>$output->output),TRUE);
        }

        $this->loadView($output);
    }

    function unProducto(){
        $productos = $_POST['productos'];     
        $productos = count(json_decode($productos));
        if(empty($productos)){            
            $this->form_validation->set_message('unProducto','Debe incluir al menos un producto');        
            return false;
        }
        //Validar si el pago es completo
    }

    //Callback de function ventas a,b insert
    function addBody($post,$primary){
        $productos = json_decode($_POST['productos']);
        $pro = array();
        //$this->db->delete('ventadetalle',array('venta'=>$primary));
        $total = 0;       
        foreach($productos as $p){
            $pro[] = $p->nombre_comercial; 
            $this->db->insert('compradetalles',array(
                'compra'=>$primary,
                'producto'=>$p->codigo,                
                'cantidad'=>$p->cantidad,
                'lote'=>'',
                'vencimiento'=>'0000-00-00',
                'precio_costo'=>$p->precio_costo,
                'por_desc'=>$p->por_desc,
                'por_venta'=>$p->por_venta,
                'precio_venta'=>$p->precio_venta,
                'total'=>$p->total
            ));
            $total+= $p->total;
        }
        $this->db->update('compras',array('productos'=>implode($pro),'total_compra'=>$total),array('id'=>$primary));        
        if($post['pago_caja_diaria']==1){
            $this->db->insert('gastos',array(
                 'cajadiaria'=>$this->user->cajadiaria,
                 'beneficiario'=>@$this->db->get_where('proveedores',array('id'=>$post['proveedor']))->row()->denominacion,
                 'concepto'=>'Compra #'.$primary,
                 'cuentas_id'=>1,
                 'fecha_gasto'=>date("Y-m-d"),                 
                 'monto'=>$total,
                 'nro_documento'=>$post['nro_factura'],
                 'user_created'=>$this->user->id,                 
            ));

            $this->db->insert('pagoproveedores',array(
                'fecha'=>date("Y-m-d H:i:s"),
                'forma_pago'=>'Efectivo',
                'comprobante'=>'Compra #'.$primary,
                'nro_recibo'=>$post['nro_factura'],
                'proveedor'=>$post['proveedor'],
                'total_abonado'=>$total,
                'totalnotacredito'=>0,
                'totalefectivo'=>$total,
                'anulado'=>0,
                'sucursal'=>$this->user->sucursal,
                'caja'=>$this->user->caja,
                'user'=>$this->user->id,
                'cajadiaria'=>$this->user->cajadiaria
            ));
            $pago = $this->db->insert_id();
            $this->db->insert('pagoproveedor_detalles',array(
                'pagoproveedor'=>$pago,
                'nro_factura'=>$post['nro_factura'],
                'compra'=>$primary,
                'fecha_factura'=>date('Y-m-d'),
                'monto'=>$total,
                'nota_credito'=>0,
                'total'=>$total
            ));
        }
    }

    function compras_detail($ventaid){
        $this->as['compras_detail'] = 'compradetalles';
        $crud = $this->crud_function('','');
        $crud->where('compra',$ventaid);
        $crud->columns('producto','cantidad','precio_compra','precio_venta','total');
        $crud->callback_column('producto',function($val,$row){
            return $this->db->get_where('productos',array('codigo'=>$row->producto))->roW()->nombre_comercial;
        }); 
        $crud->unset_add()
             ->unset_edit()
             ->unset_delete()
             ->unset_export()
             ->unset_print()
             ->unset_read();
        $crud->staticShowList = true;
        $crud = $crud->render();
        echo $crud->output;

    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
