<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once('Main.php');

class Cron extends Main {

    function __construct() {
        parent::__construct();         
    }

    function cleanData($data){
        $data = strip_tags($data);
        $data = str_replace(chr(194),"",$data);
        $data = str_replace('&#39;','\'',$data);
        $data = str_replace('&gt;','>',$data);
        $data = str_replace('&lt;','<',$data);
        $data = str_replace('&nbsp;',' ',$data);
        $data = str_replace('Â','',$data);
        $data = utf8_decode($data);
        $data = str_replace('?','',$data);     
        $data = str_replace('\n','',$data);
        $data = str_replace('\r','',$data);
        $data = str_replace('; ',';',$data);
        return trim($data);
    }
    
    function validar_stock(){
        $reporte = $this->db->get_where('newreportes',array('id'=>63));
        if($reporte->num_rows()>0){
        	/*$reporte = $reporte->row();
        	$reporte->query = $this->cleanData($reporte->query);
        	$reporte->query = str_replace('consulta=','',$reporte->query);
        	$reporte->query = trim($reporte->query);
        	$reporte->query = str_replace('|selec|','SELECT',$reporte->query); //Quitamos la validación XSS
        	$this->db->query($reporte->query);*/
        	$query = $this->db->query("
				SELECT
				productosucursal.producto as cod_producto,
				productos.nombre_comercial as producto,
				stock_consulta.stock_consulta as stock_consulta,
				productosucursal.stock as stock_inventario,
				if(stock_consulta.stock_consulta=productosucursal.stock,1,0) as verificacion
				FROM(
				SELECT
				stock.cod_producto,
				sum(stock.compra) as compra,
				sum(stock.Ventas) as venta,
				sum(stock.Ajuste_Entrada) as ajuste_entrada,
				sum(stock.Ajuste_Salida) as ajuste_salida,
				(sum(stock.compra) + sum(stock.Ajuste_Entrada)) - (sum(stock.Ventas)+sum(stock.Ajuste_Salida)) as stock_consulta
				FROM(
				SELECT 
				movimiento.cod_producto as cod_producto, 
				if(movimiento.Movimiento = 'compra',Cantidad,0) as Compra, 
				if(movimiento.Movimiento = 'venta',Cantidad,0) as Ventas, 
				if(movimiento.Movimiento = 'ajuste_entrada',Cantidad,0) as Ajuste_Entrada, 
				if(movimiento.Movimiento = 'ajuste_salida',Cantidad,0) as Ajuste_Salida 
				FROM(
				SELECT 
				'venta' as Movimiento, 
				ventas.fecha as Fecha,
				productos.codigo as cod_producto,
				ventadetalle.cantidad as Cantidad 
				FROM ventas 
				INNER JOIN ventadetalle on ventadetalle.venta = ventas.id 
				INNER JOIN productos on productos.codigo = ventadetalle.producto 
				WHERE (ventas.status = 0 or ventas.status is null) 
				UNION 
				SELECT 
				'compra' as Movimiento, 
				compras.fecha as Fecha,
				productos.codigo as cod_producto,
				compradetalles.cantidad as Cantidad 
				FROM compras 
				INNER JOIN compradetalles on compradetalles.compra = compras.id 
				INNER JOIN productos on productos.codigo = compradetalles.producto 
				WHERE (compras.status = 0 or compras.status is null)
				UNION 
				SELECT 
				'ajuste_entrada' as Movimiento, 
				entrada_productos.fecha as Fecha, 
				productos.codigo as cod_producto,
				entrada_productos_detalles.cantidad as Cantidad 
				FROM entrada_productos 
				INNER JOIN entrada_productos_detalles on entrada_productos_detalles.entrada_producto = entrada_productos.id 
				INNER JOIN productos on productos.codigo = entrada_productos_detalles.producto 
				UNION 
				SELECT 
				'ajuste_salida' as Movimiento, 
				salidas.fecha as Fecha,
				productos.codigo as cod_producto,
				salida_detalle.cantidad as Cantidad 
				FROM salidas 
				INNER JOIN salida_detalle on salida_detalle.salida = salidas.id 
				INNER JOIN productos on productos.codigo = salida_detalle.producto) as movimiento) as stock
				GROUP by stock.cod_producto) AS stock_consulta
				INNER JOIN productosucursal on productosucursal.producto = stock_consulta.cod_producto
				INNER JOIN productos ON productos.codigo = productosucursal.producto
				WHERE productos.categoria_id != 1 AND if(stock_consulta.stock_consulta=productosucursal.stock,1,0) = 0
				ORDER by verificacion asc
        	");        	
        	return $query;
        	
        }
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
