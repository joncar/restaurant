<?php $this->load->view('predesign/breadcrumb'); ?>
<div class="row">
    <? $this->load->view('includes/nav') ?>
    <article class="col-xs-12 col-sm-10">  
        <div class="alert alert-info"><b>Nota: </b> Una vez guardada una distribución a una sucursal solo se podrá aumentar el valor distribuido luego de su guardado</div>
            Distribución de productos de la orden #<?= $compra->nro_factura ?>
            <div class="row form-group">
              <label for="transaccion" class="col-sm-4 control-label">Productos de la orden</label>
              <div class="col-sm-8">   
                  
                  <select class="form-control" id="producto">
                  <?php foreach($detalles->result() as $d):?>
                      <?php if($d->cantidad>0): ?>
                      <option value="<?= $d->producto_codigo ?>" data-disponible="<?= $d->total ?>" data-cantidad="<?= $d->cantidad ?>" data-lote="<?= $d->lote ?>" data-vencimiento="<?= $d->vencimiento ?>" data-precio_venta="<?= $d->precio_venta ?>"><?= $d->producto ?></option>
                      <?php endif ?>
                  <?php endforeach ?>
                  </select>                
                  Cantidad disponible: <span id="disponible"><?= $detalles->row()->cantidad ?></span>
              </div>
            </div>
            <div class="row alert" style="display:none"></div>
            <form action="" id="formulario" onsubmit="return sendit(this)">
                <h3>Distribuir producto en Sucursales</h3>
                <?php foreach($sucursales->result() as $s): ?>                
                <div class="row">
                    <div class="col-xs-3"><b><?= $s->denominacion ?></b></div>
                    <div class="col-xs-9"><input type="number" name="sucursal_<?= $s->id ?>" id="sucursal_<?= $s->id ?>" class="sucursal form-control" value="0"></div>
                </div>
                <?php endforeach ?>
                <input type="hidden" name="compra" id="compra" value="<?= $compra->id ?>">
                <input type="hidden" name="producto" id="productoh" value="<?= $detalles->row()->producto_codigo ?>">                
                <input type="hidden" name="lote" id="lote" value="<?= $detalles->row()->lote ?>">
                <input type="hidden" name="vencimiento" id="vencimiento" value="<?= $detalles->row()->vencimiento ?>">
                <input type="hidden" name="precio_venta" id="precio_venta" value="<?= $detalles->row()->precio_venta ?>">
                <div style="margin:30px; text-align: center">
                    <button type="submit" class="btn btn-success">Guardar Cambios</button> 
                    <a href="<?= base_url($this->router->fetch_class().'/distribucion') ?>" class="btn btn-default">Volver a distribuciones</a>
                    <a href="<?= base_url($this->router->fetch_class().'/compras/add') ?>" class="btn btn-default">Volver a compras</a>
                    <a href="<?= base_url('panel/imprimir_reporte/1/'.$compra->id) ?>" target="_new" class="btn btn-default">Imprimir distribución</a>
                </div>
            </form>
            <div id="json" style="display: none"></div>
    </article>
</div>

<script>
    var disponible = <?= $detalles->row()->total ?>, dis;
     $.post('<?= base_url('json/seldistribucionproducto/'.$compra->id.'/'.$detalles->row()->producto_codigo) ?>',{},function(data){
           $("#json").html(data);
        });
    $(document).ready(function(){$("#producto").trigger('change')})
    $(document).on('change','#producto',function(){
        $("#disponible").html($(this).find(':selected').data('cantidad'));
        disponible = $(this).find(':selected').data('disponible');        
        $("#lote").val($(this).find(':selected').data('lote'));
        $("#vencimiento").val($(this).find(':selected').data('vencimiento'));
        $("#precio_venta").val($(this).find(':selected').data('precio_venta'));
        $(".sucursal").val(0);
        $("#productoh").val($(this).val());
        $.post('<?= base_url('json/seldistribucionproducto/'.$compra->id.'/') ?>/'+$(this).val(),{},function(data){
           $("#json").html(data);
        });
    });
    
    $(document).on('change','.sucursal',function(){        
        dis = disponible;
        $(".sucursal").each(function(){
            dis-=$(this).val();        
            $("#disponible").html(dis);
        })
    })
    function sendit(form){        
        if($("#producto").val()!=null){
        var data = document.getElementById('formulario');        
        data = new FormData(data);
        $.ajax({
            url:'<?= base_url('json/distribucion') ?>',
            method:'post',
            data:data,
            processData:false,
            cache: false,
            contentType: false,
            success:function(data){
                data = JSON.parse(data);
                if(data['status']){
                    $(".alert").removeClass('alert-danger').addClass('alert-success').html('Se han guardado los datos con exito').show();
                    setTimeout(function(){document.location.reload();},1000);
                }
                else
                    $(".alert").removeClass('alert-success').addClass('alert-danger').html(data['message']).show();
            }
            });
        }
        else alert("Seleccione un producto para realizar esta operación");
        return false;
    }
</script>