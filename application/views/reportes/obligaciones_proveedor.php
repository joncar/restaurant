<?php if(empty($_POST)): ?>
<? $this->load->view('predesign/datepicker'); ?>
<? $this->load->view('predesign/chosen'); ?>
<div class="container">
    <h1 align="center"> Resumen de obligaciones a proveedores</h1>
<form action="<?= base_url('reportes/obligaciones_proveedor') ?>" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Seleccione un proveedor</label>
        <?= form_dropdown_from_query('proveedor','proveedores','id','denominacion',0) ?>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Desde</label>
    <input type="text" name="desde" class="form-control datetime-input" id="desde">
  </div>  
  <div class="form-group">
    <label for="exampleInputPassword1">Hasta</label>
    <input type="text" name="hasta" class="form-control datetime-input" id="hasta">
  </div>
  <button type="submit" class="btn btn-default">Consultar reporte</button>
</form>
</div>
<?php else: ?>
    <? if(!empty($_POST['proveedor']))$proveedor = $this->db->get_where('proveedores',array('id'=>$_POST['proveedor']))->row()->denominacion; ?>
    <h1 align="center"> Resumen de obligaciones a proveedores</h1>
    <p><strong>Proveedor: </strong> <?= empty($_POST['proveedor'])?'Todos':$proveedor ?></p>
    <p><strong>Desde:</strong> <?= empty($_POST['desde'])?'Todos':$_POST['desde'] ?> <strong>Hasta:</strong> <?= empty($_POST['hasta'])?'Todos':$_POST['hasta'] ?></p>
    
    <table border="0" cellspacing="18" class="table" width="100%">
        <thead>
                <tr>
                        <th>Nro factura</th>
                        <th>Fecha Fact.</th>
                        <th>Vence Pago</th>
                        <th>Monto</th>
                        <th>Monto Nota de credito</th>
                        <th>A Pagar</th>
                </tr>
        </thead>
        <tbody>
            <?php
                $_POST['desde'] = !empty($_POST['desde'])?date("Y-m-d",strtotime(str_replace("/","-",$_POST['desde']))):'';
                $_POST['hasta'] = !empty($_POST['hasta'])?date("Y-m-d",strtotime(str_replace("/","-",$_POST['hasta']))):'';
                if(!empty($_POST['proveedor']))$this->db->where('compras.proveedor',$_POST['proveedor']);
                if(!empty($_POST['desde']))$this->db->where('compras.fecha >=',$_POST['desde']);
                if(!empty($_POST['hasta']))$this->db->where('compras.fecha <=',$_POST['hasta']);
                $this->db->where('status',0);
                $total = 0;
                $this->db->select('compras.*');                
                $this->db->order_by('compras.fecha','ASC');
                $compras = $this->db->get('compras');
            ?>
            <?php foreach($compras->result() as $c): ?>
                <tr>                        
                        <td><?= $c->nro_factura ?></td>
                        <td><?= $c->fecha ?></td>
                        <td><?= $c->vencimiento_pago ?></td>
                        <td align="right"><?= number_format($c->total_compra,0,',','.') ?> </td>
                        <td align="right">0</td>
                        <td align="right"><?= number_format($c->total_compra,0,',','.') ?> </td>
                        <? $total+= $c->total_compra; ?>
                </tr>
            <?php endforeach ?>
                <tr>
                    <th colspan="5">Total Deuda</th>
                    <th align="right"><?= number_format($total,0,',','.') ?></th>
                </tr>
        </tbody>
    </table>
<?php endif; ?>