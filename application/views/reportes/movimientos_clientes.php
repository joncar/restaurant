<?php if(empty($_POST) || empty($_POST['clientes'])): ?>
<? $this->load->view('predesign/datepicker'); ?>
<? $this->load->view('predesign/chosen'); ?>
<div class="container">
    <h1 align="center"> Resumen de movimientos de clientes</h1>
<form action="<?= base_url('reportes/movimientos_clientes') ?>" method="post">
    <div class="form-group">
    <label for="exampleInputEmail1">Seleccione un cliente</label>
        <?php $this->db->order_by('apellidos','asc'); ?>
        <?= form_dropdown_from_query('clientes','clientes','id','apellidos nombres nro_documento',0) ?>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Desde</label>
    <input type="text" name="desde" class="form-control datetime-input" id="desde">
  </div>  
  <div class="form-group">
    <label for="exampleInputPassword1">Hasta</label>
    <input type="text" name="hasta" class="form-control datetime-input" id="hasta">
  </div>
  <button type="submit" class="btn btn-default">Consultar reporte</button>
</form>
</div>
<?php else: ?>
        <?php
            $_POST['desde'] = !empty($_POST['desde'])?date("Y-m-d",strtotime(str_replace('/','-',$_POST['desde']))):'';
            $_POST['hasta'] = !empty($_POST['hasta'])?date("Y-m-d",strtotime(str_replace('/','-',$_POST['hasta']))):'';
            $where = '';
            $where.= !empty($_POST['desde'])?"WHERE DATE(fecha) >='".$_POST['desde']."'":'';
            if(!empty($_POST['hasta'])){
                $where.= empty($_POST['desde'])?"WHERE DATE(fecha) <='".$_POST['hasta']."'":" AND DATE(fecha)<='".$_POST['hasta']."'";
            }
            $query = "
                select
                DATE_FORMAT(fecha,'%d-%m-%Y') as Fecha,
                tipo,nombres as cliente,
                IF(tipo='VENTA',total_venta,0) as 'VENTA',
                IF(tipo='PAGO',total_venta,0) as 'PAGO'
                from 
                (
                select'VENTA' as tipo,
                ventas.fecha,
                clientes.nombres,
                ventas.total_venta
                from ventas
                inner join clientes on clientes.id = ventas.cliente
                where 
                clientes.id = ".$_POST['clientes']."
                AND transaccion = 2 
                AND (anulado = 0 OR anulado is NULL)
                UNION
                select
                'PAGO' as tipo,
                pagocliente.fecha,
                clientes.nombres,
                pagocliente.totalpagado
                from pagocliente
                inner join clientes on clientes.id = pagocliente.cliente
                where clientes.id = ".$_POST['clientes']." AND (anulado = 0 OR anulado is NULL)) as transacciones ".$where." order by transacciones.fecha asc
            ";
            $ventas = $this->db->query($query);
        ?>
        
    <h1 align="center"> Resumen de movimientos de clientes</h1>    
    <p style="font-size:12px;"><strong>Desde:</strong> <?= empty($_POST['desde'])?'Todos':$_POST['desde'] ?> <strong>Hasta:</strong> <?= empty($_POST['hasta'])?'Todos':$_POST['hasta'] ?> </p>
    <?php if($ventas->num_rows()>0): ?>
    <table border="0" cellspacing="18" class="table" width="100%" style="font-size:12px;">
        <thead>
                <tr>
                    <?php foreach($ventas->row() as $n=>$v): ?>
                    <th style="text-align:center;"><?= ucwords(str_replace('_',' ',$n)) ?></th>
                    <?php endforeach ?>
                    <th style="text-align:center;">Saldo</th>
                </tr>
        </thead>
        <tbody>
            <?php $totales = array(); ?>
            <?php $saldo = 0; ?>
            <?php foreach($ventas->result() as $n=>$c): ?>                
                <tr>
                        <?php foreach($c as $n2=>$v): ?>
                            <?php 
                                if(empty($totales[$n2])){
                                    $totales[$n2] = 0;
                                }
                                if(is_numeric($v)){
                                    $totales[$n2]+=$v;
                                }
                            ?>
                            <td style="text-align:center"><?= is_numeric($v)?number_format($v,0,',','.'):$v ?></td>
                        <?php endforeach ?>
                            <td style="text-align:center"><?php $saldo = ($saldo + $c->VENTA)-$c->PAGO; echo number_format($saldo,0,',','.') ?></td>
                </tr>
            <?php endforeach ?>
            <tr>
                    <?php $n = 0; ?>
                    <?php foreach($ventas->row() as $n2=>$v): ?>                        
                        <td style="text-align:<?= !is_numeric($v)?'center':'right' ?>">
                            <?php 
                                if($n!=0 &&  !empty($totales[$n2])){
                                    echo '<b>'.number_format($totales[$n2],0,',','.').'</b>';
                                }elseif($n==0){
                                    echo '<b>TOTALES</b>';
                                }
                            ?>                 
                        </td>
                        <?php $n++; ?>
                    <?php endforeach ?>
                        <td style="text-align:<?= !is_numeric($v)?'center':'right' ?>"><b>
                        <?php 
                          echo number_format($saldo,0,',','.');
                          ?>                 </b>
                    </td>
            </tr>
        </tbody>
    </table>
    <table>
        <thead>
            <tr>
                <th colspan="2">Resumen</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <?php 
                    $venta = $this->db->query('select SUM(ventas.total_venta) as total from ventas where (anulado = 0 OR anulado is NULL) AND transaccion = 2 AND cliente='.$_POST['clientes'])->row();
                    $pagos = $this->db->query('select SUM(pagocliente.totalpagado) as total from pagocliente where (anulado = 0 OR anulado is NULL) AND cliente = '.$_POST['clientes'])->row();
                ?>
                <td>Total Compra</td><td><?= number_format($venta->total,0,',','.') ?></td>
            </tr>
            <tr>
                <td>Pagos</td><td><?= number_format($pagos->total,0,',','.') ?></td>
            </tr><tr>
                <td>Saldo</td><td><?= number_format($venta->total-$pagos->total,0,',','.'); ?></td>
            </tr>
        </tbody>
    </table>
    <?php endif ?>    
<?php endif; ?>    