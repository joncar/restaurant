            <p>FARMACIA TELEFARMA<br/>
            Informe de salida de productos</p>
            <table border="0" cellspacing="2" cellpading="2">                
                <tr><td><b>Fecha: </b></td><td><?= date("d-m-Y",strtotime($venta->fecha)) ?></td><td><b>Proveedor: </b></td><td><?= $venta->proveedorname ?></td></tr>
                <tr><td><b>Motivo: </b></td><td><?= $venta->motivoname ?></td><td><b>Usuario: </b></td><td><?= $venta->usuario ?></td></tr>
            </table>
            <br/>
            <table border="1" cellspacing="4" cellpading="2">                
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Nombre</th>
                        <th>Lote</th>
                        <th>Vence</th>
                        <th>Cantidad</th>
                        <th>Precio Costo</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($detalles->result() as $d): ?>
                    <tr>
                        <td><?= $d->producto ?></td>
                        <td><?= cortar_palabras($d->nombre_comercial,5) ?></td>
                        <td><?= $d->lote ?></td>
                        <td><?= date("d-m-Y",strtotime($d->vencimiento)) ?></td>
                        <td><?= $d->cantidad ?></td>
                        <td><?= $d->precio_costo ?></td>                        
                        <td><?= $d->total ?></td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
            <p>&nbsp;</p>
            <table cellspacing="20" cellpading="20">
                    <tr>
                        <td style="text-align:center"><div style="margin:20px">___________________ <br/> Cajero</div></td>
                        <td style="text-align:center"><div style="margin:20px">___________________ <br/> Administrador</div></td>                                                
                    </tr>
            </table>
