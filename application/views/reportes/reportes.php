<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('admin.php');
class Reportes extends Admin {
        
	function __construct()
	{
		parent::__construct();  
                ini_set('memory_limit','200M');
	}

        function index($url = 'main',$page = 0)
	{
		$this->loadView('reportes');
	}          
        
        public function resumen_compras(){            
            if(empty($_POST)){
                $this->loadView('reportes/resumen_compras');
            }
            else{
                $papel = 'A4';
                $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                $html2pdf->setDefaultFont('courier');
                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/resumen_compras',array(),TRUE)));
                $html2pdf->Output('Resumen de compras.pdf');
            }
        }
        
        public function resumen_ventas(){            
            if(empty($_POST)){
                $this->loadView('reportes/resumen_ventas');
            }
            else{
                $papel = 'A4';
                $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                $html2pdf->setDefaultFont('courier');
                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/resumen_ventas',array(),TRUE)));
                $html2pdf->Output('Resumen de ventas.pdf');
                
                //$this->load->view('reportes/resumen_ventas');
            }
        }
        
        public function obligaciones_proveedor(){            
            if(empty($_POST)){
                $this->loadView('reportes/obligaciones_proveedor');
            }
            else{
                $papel = 'A4';
                $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                $html2pdf->setDefaultFont('courier');
                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/obligaciones_proveedor',array(),TRUE)));
                $html2pdf->Output('Resumen de obligaciones proveedor.pdf');
                
                //$this->load->view('reportes/obligaciones_proveedor');
            }
        }
        
        public function ventas_sucursal(){
            if(empty($_POST)){
                $this->loadView('reportes/ventas_sucursal');
            }
            else{
                $papel = 'A4';
                $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                $html2pdf->setDefaultFont('courier');
                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/ventas_sucursal',array(),TRUE)));
                $html2pdf->Output('Resumen de ventas por sucursal.pdf');
                
                //$this->load->view('reportes/ventas_sucursal');
            }
        }
        
        public function vencimientos(){
            if(empty($_POST)){
                $this->loadView('reportes/vencimientos');
            }
            else{
                $papel = 'A4';
                $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                $html2pdf->setDefaultFont('courier');
                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/vencimientos',array(),TRUE)));
                $html2pdf->Output('Resumen de vencimientos.pdf');
                
                //$this->load->view('reportes/vencimientos');
            }
        }
        
        public function saldos(){
            if(empty($_POST)){
                $this->loadView('reportes/saldos');
            }
            else{
                $papel = 'A4';
                $html2pdf = new HTML2PDF('L',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                $html2pdf->setDefaultFont('courier');
                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/saldos',array(),TRUE)));
                $html2pdf->Output('Resumen de saldo de clientes.pdf');
                
                //$this->load->view('reportes/saldos');
            }
        }
        
        public function resumen_vencimiento(){
            if(empty($_POST)){
                $this->loadView('reportes/resumen_vencimiento');
            }
            else{
                $papel = 'A4';
                $html2pdf = new HTML2PDF('L',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                $html2pdf->setDefaultFont('courier');
                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/resumen_vencimiento',array(),TRUE)));
                //$html2pdf->Output('Resumen de vencimiento.pdf');
                
                $this->load->view('reportes/resumen_vencimiento');
            }
        }
        
        public function resumen_descuentos(){
            if(empty($_POST)){
                $this->loadView('reportes/resumen_descuentos');
            }
            else{
                $papel = 'A4';
                $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                $html2pdf->setDefaultFont('courier');
                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/resumen_descuentos',array(),TRUE)));
                //$html2pdf->Output('Resumen de vencimiento.pdf');
                
                $this->load->view('reportes/resumen_descuentos');
            }
        }
        
        public function listado_inventario(){
            if(empty($_POST)){
                $this->loadView('reportes/listado_inventario');
            }
            else{
                $papel = 'A4';
                $html2pdf = new HTML2PDF('L',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                $html2pdf->setDefaultFont('courier');
                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/listado_inventario',array(),TRUE)));
                $html2pdf->Output('Listado de inventario.pdf');
                
                $this->load->view('reportes/listado_inventario');
            }
        }
        
        public function listado_inventario_total(){
            if(empty($_POST)){
                $this->loadView('reportes/listado_inventario_total');
            }
            else{
                $papel = 'A4';
                $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                $html2pdf->setDefaultFont('courier');
                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/listado_inventario_total',array(),TRUE)));
                $html2pdf->Output('Listado de inventario.pdf');
                
                $this->load->view('reportes/listado_inventario_total');
            }
        }
        public function listado_inventario_categoria(){
            if(empty($_POST)){
                $this->loadView('reportes/listado_inventario_categoria');
            }
            else{
                $papel = 'A4';
                $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                $html2pdf->setDefaultFont('courier');
                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/listado_inventario_categoria',array(),TRUE)));
                $html2pdf->Output('Listado de inventario.pdf','D');
                
                //$this->load->view('reportes/listado_inventario_categoria');
            }
        }
        
        public function listado_ventas_clientes_creditos(){
            if(empty($_POST)){
                $this->loadView('reportes/listado_ventas_clientes_creditos');
            }
            else{
                $papel = 'A4';
                $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                $html2pdf->setDefaultFont('courier');
                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/listado_ventas_clientes_creditos',array(),TRUE)));
                $html2pdf->Output('Listado de ventas por cliente.pdf','D');
                
                //$this->load->view('reportes/listado_ventas_clientes_creditos');
            }
        }
        
        public function listado_ventas_clientes(){
            if(empty($_POST)){
                $this->loadView('reportes/listado_ventas_clientes');
            }
            else{
                $papel = 'A4';
                $html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                $html2pdf->setDefaultFont('courier');
                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/listado_ventas_clientes',array(),TRUE)));
                $html2pdf->Output('Listado de ventas por cliente.pdf','D');
                
                //$this->load->view('reportes/listado_ventas_clientes');
            }
        }
        
        public function detalle_ventas(){
            if(empty($_POST)){
                $this->loadView('reportes/detalle_ventas');
            }
            else{
                $papel = 'A4';
                $html2pdf = new HTML2PDF('L',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                $html2pdf->setDefaultFont('courier');
                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/detalle_ventas',array(),TRUE)));
                $html2pdf->Output('Detalle-de-ventas-por-dia.pdf','D');
                
                //$this->load->view('reportes/detalle_ventas');
            }
        }
        /*Cruds*/ 
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */