<?php if(empty($_POST)): ?>
<? $this->load->view('predesign/datepicker'); ?>
<? $this->load->view('predesign/chosen'); ?>
<div class="container">
    <h1 align="center"> Resumen de facturas para contador</h1>
<form action="<?= base_url('reportes/resumen_facturas_contador') ?>" method="post">
  <div class="form-group">
    <label for="exampleInputPassword1">Desde</label>
    <input type="text" name="desde" class="form-control datetime-input" id="desde">
  </div>  
  <div class="form-group">
    <label for="exampleInputPassword1">Hasta</label>
    <input type="text" name="hasta" class="form-control datetime-input" id="hasta">
  </div>
  <div>
    <label class="radio-inline">
        <input type="radio" name="tipo" id="inlineRadio1" value="pdf" checked=""> PDF
    </label>
    <label class="radio-inline">
       <input type="radio" name="tipo" id="inlineRadio2" value="csv"> EXCEL
    </label>
  </div>
  <button type="submit" class="btn btn-default">Consultar reporte</button>
</form>
</div>
<?php else: ?>    
        <?php
            $_POST['desde'] = date("Y-m-d",strtotime(str_replace('/','-',$_POST['desde'])));
            $_POST['hasta'] = date("Y-m-d",strtotime(str_replace('/','-',$_POST['hasta'])));
            $where = '';
            $where.= !empty($_POST['desde'])?"WHERE vd.fecha >='".$_POST['desde']."'":'';
            if(!empty($_POST['hasta'])){
                $where.= empty($_POST['desde'])?"WHERE vd.fecha <='".$_POST['hasta']."'":" AND vd.fecha<='".$_POST['hasta']."'";
            }
            $query = "
                SELECT 
                vd.id as ID_Factura,
                substring(vd.nro_factura,1,3) as serie,
                substring(vd.nro_factura,4) as factura,                 
                clientes.nro_documento as Cedula,
                clientes.nombres as Nombre,
                clientes.apellidos as Apellido,
                tipotransaccion.denominacion as Condicion,
                FORMAT(vd.total_venta,0,'de_DE') as total_venta,
                (
                        select IF(SUM(totalcondesc/22) IS NULL,0,TRUNCATE(SUM(totalcondesc/22),0)) from ventadetalle where venta = vd.id AND iva = 10
                ) as 'iva_10%',
                (
                        select IF(SUM(totalcondesc/11) IS NULL,0,TRUNCATE(SUM(totalcondesc/11),0)) from ventadetalle where venta = vd.id AND iva = 5
                ) as 'iva_5%',
                (
                        select IF(SUM(totalcondesc) IS NULL,0,TRUNCATE(SUM(totalcondesc),0)) from ventadetalle where venta = vd.id AND iva = 0
                ) as 'exenta',
                FORMAT(vd.total_descuentos,0,'de_DE') as total_descuentos
                FROM `ventas` vd
                INNER JOIN clientes ON clientes.id = vd.cliente
                INNER JOIN tipotransaccion ON tipotransaccion.id = vd.transaccion
                ".$where."
            ";
            $ventas = $this->db->query($query);
        ?>
    <?php if($_POST['tipo']=='pdf'): ?>
    <h1 align="center"> Resumen de facturas para contador</h1>    
    <p style="font-size:12px;"><strong>Desde:</strong> <?= empty($_POST['desde'])?'Todos':$_POST['desde'] ?> <strong>Hasta:</strong> <?= empty($_POST['hasta'])?'Todos':$_POST['hasta'] ?> </p>
    <?php if($ventas->num_rows()>0): ?>
        <table border="0" cellspacing="18" class="table" width="100%" style="font-size:12px;">
            <thead>
                    <tr>
                        <?php foreach($ventas->row() as $n=>$v): ?>
                        <th style="text-align:center;"><?= ucwords(str_replace('_',' ',$n)) ?></th>
                        <?php endforeach ?>
                    </tr>
            </thead>
            <tbody>
                <?php $totales = array(); ?>
                <?php foreach($ventas->result() as $n=>$c): ?>                
                    <tr>
                            <?php foreach($c as $n2=>$v): ?>
                                <?php 
                                    if(empty($totales[$n2])){
                                        $totales[$n2] = 0;
                                    }
                                    if(is_numeric($v)){
                                        $totales[$n2]+=$v;
                                    }
                                ?>
                                <td style="text-align:center"><?= $v ?></td>
                            <?php endforeach ?>
                    </tr>
                <?php endforeach ?>
                <tr>
                    <?php $n = 0; ?>
                    <?php foreach($ventas->row() as $n2=>$v): ?>                        
                        <td style="text-align:<?= !is_numeric($v)?'center':'right' ?>">
                            <?php 
                                if($n!=0 && $n2!='cedula' && !empty($totales[$n2])){
                                    echo '<b>'.$totales[$n2].'</b>';
                                }elseif($n==0){
                                    echo '<b>TOTALES</b>';
                                }
                            ?>                 
                        </td>
                        <?php $n++; ?>
                    <?php endforeach ?>
                </tr>
            </tbody>
        </table>
        <?php endif ?>
    <?php else: ?>
    <?php 
        $string_to_export = "";        
        if($ventas->num_rows()>0){
            //Cabecera
            foreach($ventas->row() as $n=>$v){
                $string_to_export.= $n.'\t';
            }
            $string_to_export.= '\n';
            //Cuerpo
            foreach($ventas->result() as $v){
                foreach($v as $n=>$v2){
                    $string_to_export.= $v2.'\t';
                }
                $string_to_export.= '\n';
            }
            
            $string_to_export = "\xFF\xFE" .mb_convert_encoding($string_to_export, 'UTF-16LE', 'UTF-8');
            $filename = "export-".date("Y-m-d_H:i:s").".xls";
            header('Content-type: application/vnd.ms-excel;charset=UTF-16LE');
            header('Content-Disposition: attachment; filename='.$filename);
            header("Cache-Control: no-cache");
            echo $string_to_export;
        }
    ?>
    <?php endif ?>    
<?php endif; ?>