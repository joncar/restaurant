<?php if(empty($_POST)): ?>
<? $this->load->view('predesign/datepicker'); ?>
<? $this->load->view('predesign/chosen'); ?>
<div class="container">
    <h1 align="center"> Detalle de Ventas por día</h1>
<form action="<?= base_url('reportes/detalle_ventas2') ?>" method="post">
  <div class="form-group">
    <label for="exampleInputPassword1">Desde</label>
    <input type="text" name="desde" class="form-control datetime-input" id="desde">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Hasta</label>
    <input type="text" name="hasta" class="form-control datetime-input" id="hasta">
  </div>
  <button type="submit" class="btn btn-default">Consultar reporte</button>
</form>
</div>
<?php else: ?>    
    <?php if(!empty($_SESSION['sucursal'])){
        $sucursal = $this->db->get_where('sucursales',array('id'=>$_SESSION['sucursal']))->row()->denominacion;    
        $caja = $this->db->get_where('cajas',array('id'=>$_SESSION['caja']))->row()->denominacion;    
    } ?>
    <h1 align="center"> Detalle de Ventas por día</h1>    
    <p><strong>Sucursal: </strong> <?= empty($_SESSION['sucursal'])?'Todos':$sucursal ?></p>    
    <p><strong>Caja: </strong> <?= empty($_SESSION['caja'])?'Todos':$caja ?></p>    
    <p><strong>Desde:</strong> <?= empty($_POST['desde'])?'Todos':$_POST['desde'] ?> <strong>Hasta:</strong> <?= empty($_POST['hasta'])?'Todos':$_POST['hasta'] ?></p>
    <?php
                if(!empty($_POST['desde']) && !empty($_POST['hasta'])){
                    $this->db->where('Date(ventas.fecha) between \''.date("Y-m-d",strtotime(str_replace('/','-',$_POST['desde']))).'\' AND \''.date("Y-m-d",strtotime(str_replace('/','-',$_POST['hasta']))).'\'',null,TRUE);
                }                
                if(!empty($_POST['sucursal']))$this->db->where('ventas.sucursal',$_POST['sucursal']);
                $this->db->where('ventas.status != ',-1);
                $this->db->where('cajas.id',$_SESSION['caja']);
                $this->db->select('
                        CONCAT(user.nombre," ",user.apellido) as empleado,
                        CONCAT(clientes.nombres," ",clientes.apellidos) as cliente,
                        productos.nombre_comercial as productonombre,
                        tipotransaccion.denominacion as transaccion,
                        formapago.denominacion as formapago, 
                        cajas.denominacion as caja,
                        ventas.fecha, 
                        ventas.transaccion,
                        ventadetalle.venta as ventaid,
                        ventadetalle.producto, '
                        . 'ventadetalle.lote, '
                        . 'ventadetalle.cantidad, '
                        . 'ventadetalle.precioventa, '
                        . 'ventadetalle.pordesc, '
                        . 'ventadetalle.totalcondesc',FALSE);
                $this->db->join('ventas','ventas.id = ventadetalle.venta')
                               ->join('cajas','cajas.id = ventas.caja')
                               ->join('user','user.id = ventas.usuario','left')
                               ->join('clientes','clientes.id = ventas.cliente')
                               ->join('tipotransaccion','tipotransaccion.id = ventas.transaccion')
                               ->join('formapago','formapago.id = ventas.forma_pago')
                               ->join('productos','productos.codigo = ventadetalle.producto');
                $ventas = $this->db->get('ventadetalle');
            ?>
    <table border="0" cellspacing="0" class="table" style="font-size:11px;">
        <thead>
                <tr>
                    <th style="width:80px">Fecha</th>
                    <th style="width:80px">Hora</th>
                    <th style="width:80px">Cliente</th>
                    <th style="width:100px">Cod. Artículo</th>                        
                    <th style="width:100px">Nombre Artículo</th>
                    <th style="width:50px">Cond.</th>
                    <th style="width:50px">Lote</th>
                    <th style="width:50px">Cant.</th>                        
                    <th style="width:80px; text-align:right;">Precio Lista</th>                        
                    <th style="width:80px; text-align:right;">Precio Neto.</th>       
                    <th style="width:80px; text-align:right;">Descuento.</th>
                    <th style="width:100px; text-align:right;">Total</th>                        
                </tr>
        </thead>
        <tbody>
            
            <?php 
                $total = 0; 
                $totaldescuento = 0; 
                $totalcontado = 0;
                $totalcredito = 0;
             ?>
            <?php foreach($ventas->result() as $c): ?>
                <?php 
                    $descuento = ($c->precioventa*($c->pordesc/100)); 
                    $total+= $c->totalcondesc; 
                    $totaldescuento+= $descuento;
                    $totalcontado+= $c->transaccion==1?$c->totalcondesc:0;
                    $totalcredito+= $c->transaccion==2?$c->totalcondesc:0;
                 ?>
                <tr>
                        <td><?= date("d-m-Y",strtotime($c->fecha)) ?></td>
                        <td><?= date("H:i:s",strtotime($c->fecha)) ?></td>
                        <td><?= substr($c->cliente,0,8) ?></td>
                        <td><?= $c->producto ?></td>                        
                        <td><?= cortar_palabras($c->productonombre,3) ?></td>
                        <td><?= $c->transaccion==1?'Cont':'Cred' ?></td>
                        <td><?= $c->lote ?></td>
                        <td><?= $c->cantidad ?></td>                        
                        <td style="text-align:right;"><?= number_format($c->precioventa,2,',','.') ?></td>                        
                        <td style="text-align:right;"><?= number_format($c->precioventa-$descuento,2,',','.') ?></td>                        
                        <td style="text-align:right;"><?= number_format($descuento,2,',','.') ?></td>                        
                        <td style="text-align:right;"><?= number_format($c->totalcondesc,2,',','.') ?></td>                        
                </tr>
            <?php endforeach ?>
                <tr>
                    <td colspan="10" style="text-align:right; font-weight: bold">Total: </td>                        
                        <td style="text-align:right; font-weight: bold"><?= number_format($total,2,',','.') ?></td>
                </tr>
                <tr>
                    <td colspan="10" style="text-align:right; font-weight: bold">Total Descuento: </td>                        
                        <td style="text-align:right; font-weight: bold"><?= number_format($totaldescuento,2,',','.') ?></td>
                </tr>
                <tr>
                    <td colspan="10" style="text-align:right; font-weight: bold">Total Contado: </td>                        
                        <td style="text-align:right; font-weight: bold"><?= number_format($totalcontado,2,',','.') ?></td>
                </tr>
                <tr>
                    <td colspan="10" style="text-align:right; font-weight: bold">Total Crédito: </td>                        
                        <td style="text-align:right; font-weight: bold"><?= number_format($totalcredito,2,',','.') ?></td>
                </tr>
                <tr>
                    <td colspan="10" style="text-align:right; font-weight: bold">Total Pago Clientes: </td>                        
                        <td style="text-align:right; font-weight: bold"><?php
                        $this->db->select('                        
                            SUM(pagocliente.totalpagado) as total
                        ');
                        $this->db->from('pagocliente');
                        
                        if(!empty($_SESSION['sucursal'])){
                            $this->db->where('pagocliente.sucursal',$_SESSION['sucursal']);
                        }
                        if(!empty($_POST['desde'])){
                            $this->db->where('DATE(pagocliente.fecha) >=',date("Y-m-d",strtotime(str_replace('/','-',$_POST['desde']))));
                        }

                        if(!empty($_POST['hasta'])){
                            $this->db->where('DATE(pagocliente.fecha) <=',date("Y-m-d",strtotime(str_replace('/','-',$_POST['hasta']))));
                        }
                        $this->db->where('anulado',0);
                        $total = $this->db->get()->row()->total;
                        $total = $total==null?0:$total;
                        echo number_format($total,2,',','.') 
                                
                       ?></td>
                </tr>
        </tbody>
    </table>
<?php endif; ?>