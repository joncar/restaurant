<div class="alert <?= !empty($_SESSION['msj'])?'alert-success':'' ?>" style="<?= !empty($_SESSION['msj'])?'':'display:none' ?>"><?= !empty($_SESSION['msj'])?$_SESSION['msj']:'' ?></div>
<?php $_SESSION['msj'] = !empty($_SESSION['msj'])?'':'' ?>
<form class="form-horizontal" id='formulario' onsubmit="return val_send(this)" role="form">
    <div class="well">
        <div class="row">
            <div class="col-xs-6">
                <div class="form-group ui-widget">
                  <label for="proveedor" class="col-sm-4 control-label">Destino: </label>
                  <div class="col-sm-8" id="cliente_div">
                      <? $sel = empty($transferencia)?0:$transferencia->sucursal_destino; ?>                      
                      <?= form_dropdown_from_query('sucursal_destino','sucursales','id','denominacion',$sel,'id="sucursal_destino"') ?>
                  </div>
                </div>
            </div>       
        </div>
        <div class="row">
            <div class="col-xs-6">
                <div class="form-group ui-widget">
                  <label for="proveedor" class="col-sm-4 control-label">
                        Origen: 
                  </label>
                  <div class="col-sm-8" id="cliente_div">
                      <? $sel = empty($transferencia)?0:$transferencia->sucursal_origen; ?>
                      <?= form_dropdown_from_query('sucursal_origen','sucursales','id','denominacion',$sel,'id="sucursal_origen"') ?>
                  </div>
                </div>
            </div> 
            <div class="col-xs-6">
                <div class="form-group">
                  <label for="proveedor" class="col-sm-4 control-label">Fecha: </label>
                  <div class="col-sm-8">
                      <input type="text" name="fecha_solicitud" value="<?= empty($venta)?date("d/m/Y H:i:s"):date("d/m/Y H:i:s",strtotime($transferencia->fecha_solicitud)) ?>" id="fecha_solicitud" class="datetime-input form-control">
                      <?php if(!empty($transferencia)): ?>
                      <input type="hidden" name="id" value="<?= $transferencia->id ?>">
                      <?php endif ?>
                  </div>
                </div>
            </div>         
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">Detalle de pedido <a href="javascript:advancesearch()" id="busqueda_avanzada" class="btn btn-default">Busqueda avanzada de productos</a></div>
        <div class="col-xs-12">
            <table class="table table-striped" style="font-size:12px;" cellspacing="2" id="detall">
                <thead>
                    <tr>
                        <th style="width:30%">Código</th>
                        <th style="width:30%">Nombre artículo</th>
                        <th style="width:10%">Lote</th>                        
                        <th style="width:10%">Cant.</th>                        
                        <th style="text-align:center">Acciones</th>
                    </tr>
                </thead>
                <tbody> 
                    <?php $filas = 0; ?>
                    <?php if(!empty($transferencia)): ?>
                    <?php $filas = $detalles->num_rows(); ?>
                        <?php foreach($detalles->result() as $n=>$d): ?>
                            <tr>
                                <td><input name="codigo_<?= $n ?>" data-name="codigo" type="text" class="form-control codigo" placeholder="Codigo" value="<?= $d->producto ?>"></td>
                                <td><input name="producto_<?= $n ?>" data-name="producto" type="text" class="form-control producto" readonly placeholder="Nombre" value="<?= $this->db->get_where('productos',array('codigo'=>$d->producto))->row()->nombre_comercial ?>"></td>
                                <td><input name="lote_<?= $n ?>" data-name="lote" type="text" class="form-control lote" placeholder="Lote"  value="<?= $d->lote ?>"></td>                      
                                <td><input name="cantidad_<?= $n ?>" data-name="cantidad" type="text" class="form-control cantidad" placeholder="Cantidad"  value="<?= $d->cantidad ?>"></td>                                
                                <td><p align="center">
                                        <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
                                        <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
                                    </p></td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                    <tr>
                        <td><input name="codigo_<?= $filas ?>" data-name="codigo" type="text" class="form-control codigo" placeholder="Codigo"></td>
                        <td><input name="producto_<?= $filas ?>" data-name="producto" type="text" class="form-control producto" readonly placeholder="Nombre"></td>
                        <td><select name="lote_<?= $filas ?>" data-name="lote" class="lote form-control" id="lote"></select></td>                      
                        <td><input name="cantidad_<?= $filas ?>" data-name="cantidad" type="text" class="form-control cantidad" placeholder="Cantidad"></td>
                        <td><p align="center">
                                <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
                                <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
                            </p></td>
                    </tr>
                </tbody>
            </table>
        </div>       
    </div>
    <div class="row" style="margin-top:40px">
        <input type="hidden" name="user_id" value="<?= $_SESSION['user'] ?>">
        <button id="guardar" type="submit" class="btn btn-success">Enviar pedido</button>        
        <a href="javascript:imprimir()" id="buttonPrint" style="display:<?= !empty($transferencia)?'':'none' ?>" class="btn btn-default buttonPrint">Imprimir</a>
    </div>
</form>
<?php $this->load->view('predesign/datepicker',array('scripts'=>'a')) ?>
<?= $this->load->view('predesign/chosen.php') ?>
<script src="<?= base_url('js/jquery.print.js') ?>"></script>
<script>
    var callbackAfterAddRow = function(){};
    var bandera = 0;
    function addrow(obj){
        var row = $(obj).parents('tr');
            row.after('<tr>'+row.html()+'</tr>');
            $(".total").attr('readonly',true);
            $(".hasDatepicker").removeAttr('id').removeClass('hasDatepicker');            
            date_init_calendar();
            $("tbody tr:last-child").find('.lote').parents('td').html('<select data-name="lote" class="lote form-control"></select>');
            refreshNames(obj);
    }
    function removerow(obj){
        var o = $(obj).parents('tbody');
        $(obj).parent('td').parent('tr').remove();
        refreshNames(o);
    }
    
    function refreshNames(obj){
        bandera = 0;
        $(obj).parents('table').find('tr').each(function(){
            console.log('tr');
            $(this).find('input, select').each(function(){
               $(this).attr('name',$(this).data('name')+'_'+(bandera-1)); 
            });
            bandera+=1;
        });
    }
    //Eventos
    $(document).on('keydown','input',function(event){        
        if (event.which == 13){
            if($(this).hasClass('total')){
                addrow($(this));
            }
            var inputs = $(this).parents("form").eq(0).find(":input");
            var idx = inputs.index(this);
            if (idx == inputs.length - 1) {
                inputs[0].select()
            } 
            else if($(this).val()!='' && $(this).hasClass('codigo')){
                $(this).trigger('change');                
            }
            else if($(this).val()=='' && $(this).hasClass('codigo')){
                return false;
            }
            else
            {
                inputs[idx + 1].focus(); //  handles submit buttons
                inputs[idx + 1].select();
            }
            return false;
        }
    });
    $("body").on('click','.addrow',function(e){
        e.preventDefault();        
        addrow($(this).parent('p'));
    })

    $("body").on('click','.remrow',function(e){
        e.preventDefault();
        removerow($(this).parent('p'));
    }); 
    
   $("body").on('change','.codigo',function(e){
       e.stopPropagation();
       if($(this).val()!=''){
        focused = $(this);
        elements = $(".codigo").length;
        focusedcode = $(this).val();
        focusedlote = $(this).parents('tr').find('.lote').val();
        lt = '';
        i = 0;
        $(".codigo").each(function(obj){
            i++;
            if($(this).val()==focusedcode && $(this).parents('tr').find('.lote').val() == focusedlote && !$(this).is(focused))
            {
                row = $(this).parent().parent().find('.cantidad');
                row.val(parseInt(row.val())+1);
                focused.parents('tr').find('input').val('');
            }
            
            else if(i==elements && $(this).val()!=''){
                _getproduct($(this).val(),lt)                
            }
               
        });
        }
    });
    
    function _getproduct(val,lote){
        $.post('<?= base_url('json/getProduct') ?>/'+$("#sucursal_origen").val(),{codigo:val,lote:lote},function(data){
            data = JSON.parse(data);
            data = data['producto'];
            if(data!=''){
            focused = focused.parent().parent();
            focused.find('.producto').val(data['nombre_comercial'])
            focused.find('.cantidad').val(1);
            focused.find('.lote').parents('td').html(data['lotelist']);
            focused.find('.lote').attr('data-cantidad',data['loted']);
            focused.find('.precio_venta').val(data['precio_venta']);
            focused.find('.por_venta').val(data['descuentos']);
            focused.find('.precio_desc').val(data['descuentos']);
            focused.find('.total').val(data['precio_venta']);  
            focused.find('.ivah').val(data['iva_id']);
            focused.find('.disponible').val(data['disponible']);
            $(document).trigger('total');
            callbackAfterAddRow();
            callbackAfterAddRow = function(){}
            addrow(focused.find('a'));
            $("tbody tr").last().find('.codigo').focus();
            }
            else{
                emergente('El producto ingresado no se encuentra registrado. <a target="_new" href="<?= base_url($this->router->fetch_class().'/productos/add/json') ?>/'+focused.val()+'">¿Desea registrar uno nuevo?</a>');                        
            }                                        
        });
    }
    
    function val_send(form){
        $(".mask").show();
        <?php if(!empty($transferencia) && $transferencia->sucursal_origen==$_SESSION['sucursal']): ?>
                emergente('No esta autorizado para realizar dicha acción');
        <?php else: ?>
        var data = form;        
        data = new FormData(data);
        data.append('filas',$("#detall tbody").find('tr').length);
        $("#guardar").attr('disabled','disabled');
        $.ajax({
            url:'<?= empty($transferencia)?base_url('json/transferencias'):base_url('json/transferencias/edit') ?>',
            method:'post',
            data:data,
            processData:false,
            cache: false,
            contentType: false,
            success:function(data){
                data = JSON.parse(data);                
                if(data['status']){
                    document.location.reload();
                }
                else{
                    $(".alert").removeClass('alert-success').addClass('alert-danger').html(data['message']).show();
                    $(".mask").hide();
                }
                $("#guardar").removeAttr('disabled');
            },
            error:function(data){
                alert('Ha ocurrido un error');
                $(".mask").hide();
            }
            });
            <?php endif ?>
            return false;
    }
    
    function advancesearch()
    {
        $.post('<?= base_url('json/searchProduct') ?>/'+$("#sucursal_origen").val(),{},function(data){
            emergente(data);
        });
    }
    
    function selCod(cod,lote){
        $("tbody tr:last-child").find('.codigo').val(cod);
        $("tbody tr:last-child").find('.codigo').trigger('change');
        if(lote===''){
            $("tbody tr:last-child").find('.lote').val(lote);
        }
        callbackAfterAddRow = function(){
            if(lote!==''){
                $("tbody tr:last-child").find('.lote').val(lote);
            }
        };
        $('#myModal').modal('hide');
    }
    
    
    <?php if(!empty($transferencia->id)): ?>
    function imprimir(){
        window.open('<?= base_url('movimientos/'.$this->router->fetch_class()) ?>/transferencias/imprimir/<?= $transferencia->id ?>');
    }
    <?php else: ?>
     function imprimir(codigo){
         window.open('<?= base_url('movimientos/'.$this->router->fetch_class()) ?>/transferencias/imprimir/'+codigo);
     }      
     function showButton(codigo){
            $("#buttonPrint").attr('href','javascript:imprimir('+codigo+')');
            $("#buttonticket").attr('href','javascript:imprimirTicket('+codigo+')');
            $(".buttonPrint").show();
     }
    <?php endif ?>
</script>
<div class="mask" style="background:rgba(0,0,0,.8); width:100%; height:100%; position:fixed; top:0px; left:0px; display:none;"></div>