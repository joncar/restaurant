<?php if($this->user->log): ?>

<div class="breadcrumbs" id="breadcrumbs">

        <ul class="breadcrumb">

                <li>

                        <i class="ace-icon fa fa-home home-icon"></i>

                        <a href="#">Home</a>

                </li>

                <li class="active"><?= empty($title)?'Escritorio':$title ?></li>
                <li>
                	ID caja: <span style="color:blue"><?= @$this->user->caja ?></span> | 
					ID C.Diaria: <span style="color:blue"><?= @$this->user->cajadiaria ?></span> | 
					Usuario: <span style="color:blue"><?= @$this->user->nombre ?></span> | 
					Sucursal: <span style="color:blue"><?= @$this->user->sucursalnombre ?></span> | 
					Fecha: <span style="color:blue" id="fechaSistema"><?= date("d/m/Y H:i")  ?></span>
                </li>

        </ul><!-- /.breadcrumb -->

</div>

<?php endif ?>