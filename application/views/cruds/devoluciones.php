<?= $output ?>
<script>
    $(document).ready(function(){
        $("#field-venta").change(function(){
            $.post('<?= base_url('panel/getVenta') ?>',{id:$(this).val()},function(data){
                data = JSON.parse(data);
                $("#field-monto").val(data['totalventa']);
            });
        });
        
        if($("#field-tipo_devolucion").val()==2)
            $("#caja_field_box").hide();
        else if($("#field-tipo_devolucion").val()==1)
            $("#status_field_box").hide();
        else{
            $("#caja_field_box").hide();
            $("#status_field_box").hide();
        }
            
        $("#field-tipo_devolucion").change(function(){
            if($(this).val()==1){
                $("#caja_field_box").show();
                $("#status_field_box").hide();
            }
            else{
                $("#caja_field_box").hide();
                $("#status_field_box").show();
            }
        });
    })
</script>