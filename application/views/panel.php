<body class="no-skin">
        <?php $this->load->view('includes/header') ?>
    <div class="main-container" id="main-container">
        <?php $this->load->view('includes/sidebar') ?>
        <div class="main-content">
            <div class="main-content-inner">
                <?php $this->load->view('includes/breadcum') ?>
                <div class="page-content">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php
                                if(empty($crud)):
                                $dashboard = $this->user->pagina_principal;
                                $dashboard = empty($dashboard)?'dashboard':$dashboard;
                                $dashboard = $this->load->view($dashboard,array(),TRUE,'dashboards');
                                endif;
                            ?>
                            <?= empty($crud) ? $dashboard: $this->load->view('cruds/' . $crud) ?>                
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div>
        </div><!-- /.main-content -->           
    </div><!-- /.main-container -->
    <script src="<?= base_url("js/ace.min.js") ?>"></script>
    <script src="<?= base_url("js/jquery-ui.custom.min.js") ?>"></script>   
    <script src="<?= base_url("js/ace-elements.min.js") ?>"></script>
</body>