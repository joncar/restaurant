function emergente(data,xs,ys,boton,header){


  var x = (xs==undefined)?(window.innerWidth/2)-325:xs;
  var y = (ys==undefined)?(window.innerHeight/2):ys;
  var b = (boton==undefined || boton)?true:false;
  var h = (header==undefined)?'Mensaje':header;
  if($(".modal").html()==undefined){
  $('body,html').animate({scrollTop: 0}, 800);
  var str = '';
            var str = '<!-- Modal -->'+
            '<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
            '<div class="modal-dialog">'+
            '<div class="modal-content">'+
            '<div class="modal-body">'+
            data+
            '</div>'+
            '<div class="modal-footer">'+
            '<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>'+
            '</div>'+
            '</div><!-- /.modal-content -->'+
            '</div><!-- /.modal-dialog -->'+
            '</div><!-- /.modal -->';
            $("body").append(str);
            $("#myModal").modal("toggle");
  }
  else
  {
    $(".modal-body").html(data);
                    if($("#myModal").css('display')=='none')
                        $("#myModal").modal("toggle");
  }
}

function autocomplete(inp,url) {  
  this.inp = inp;
  this.url = url;
  this.ajax = undefined;
  this.val = '';  
  this.list = inp.parent().find('ul');
  this.events = function(){
    var l = this;
    this.inp.on('keyup',function(e){
      if($(this).val().length>3){
        l.list.show();
        l.val = $(this).val();
        l.search();
      }else{
        l.list.hide();
        l.val = '';
      }
    });

    this.inp.on('focus',function(e){
      if($(this).val().length>3){
        l.list.show();
        l.val = $(this).val();
      }
    });

    this.inp.on('blur',function(){
      l.list.hide();
      l.val = '';
    });
  }

  this.search = function(){
    var l = this;
    this.list.html('<li><a href="#">Buscando por favor espere...</a></li>');
    if(this.val!=''){
      if(typeof(this.ajax)!='undefined'){
        this.ajax.abort();
        this.ajax = undefined;
      }
      this.ajax = $.post(URL+url,{
        'q':this.val
      },function(data){
        data = JSON.parse(data);
        if(data.length>0){
          var str = '';
          for(var i in data){
            str+= '<li><a href="'+data[i].link+'">'+data[i].text+'</a></li>';
          }
          l.list.html(str);
        }else{
          l.list.html('<li><a href="#">No se encontraron resultados para mostrar</a></li>');
        }
      });
    }
  }

  this.events();
}

function remoteConnection(url,data,callback){        
    return $.ajax({
        url: URI+url,
        data: data,
        context: document.body,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success:callback
    });
};

function insertar(url,datos,resultId,callback,callbackError){
    $(document).find(resultId).removeClass('alert alert-danger').html('');
    $(document).find(resultId).addClass('alert alert-info').html('Cargando, por favor espere');
    $("button[type=submit]").attr('disabled',true);
    var uri = url.replace('insert','insert_validation');
    uri = uri.replace('update','update_validation');        
    if(!(datos instanceof FormData) && (datos instanceof HTMLFormElement)){
        datos = new FormData(datos);
    }else if(!(datos instanceof FormData)){
        var d = new FormData();
        for(var i in datos){
            d.append(i,datos[i]);
        }
        datos = d;
        console.log(datos);
    }


    remoteConnection(uri,datos,function(data){
        $("button[type=submit]").attr('disabled',false);
        data = $(data).text();
        data = JSON.parse(data);
        
        if(data.success){
          remoteConnection(url,datos,function(data){
            data = $(data).text();
            console.log(data);
            data = JSON.parse(data);
            if(typeof(callback)=='function'){
                callback(data);
            }else{
                $(document).find(resultId).removeClass('alert').removeClass('alert-info');
                $(document).find(resultId).addClass('alert alert-success').html(data.success_message);
            }
          });
        }else{
          $(document).find(resultId).removeClass('alert').removeClass('alert-info');
          $(document).find(resultId).addClass('alert alert-danger').html(data.error_message);
          if(typeof(callbackError)!=='undefined'){
            callbackError(data);
          }else{
            $(document).find(resultId).removeClass('alert').removeClass('alert-info');
            $(document).find(resultId).addClass('alert alert-danger').html(data.error_message);
          }
        }
    });
}

function sendForm(form,divResponseId){
    divResponseId = typeof(divResponseId)==='undefined'?'#response':divResponseId;
    var url = $(form).attr('action');
    var response = $(form).find(divResponseId);
    var data = new FormData(form);
    remoteConnection(url,data,function(data){
        if(response!==undefined){
            response.html(data);
        }else{
            console.log(response);
        }
    });
    return false;
}